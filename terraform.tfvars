properties = [{
    name = "BatchApps",
    region = "us-east-1",
    amiId = "ami-XXXXXXXXXXXXXXX",
    instanceType = "t3.large",
    subnetId = "subnet-XXXXXXXXXXXXXXXXX",
    securityGroup = ["sg-XXXXXX"],
    iam = "read-only-access", 
    keyName = "Virginia_Staging_Keypair",
    privIp = [],
    volumeSize = "20",
    no_of_instances = "1",
    file = ["user-data.batchapps-leader"]
}, 
{
    name = "Services",
    region = "us-east-1",
    amiId = "ami-XXXXXXXXXXXXXXX", 
    instanceType = "c5.xlarge",
    subnetId = "subnet-XXXXXXXXXXXXXXXX",
    securityGroup = ["sg-XXXXXX"],
    iam = "read-only-access", 
    keyName = "Virginia_Staging_Keypair",
    privIp = ["10.0.8.61","10.0.8.62","10.0.8.63","10.0.8.64","10.0.8.65","10.0.8.66"],
    volumeSize = "30",
    no_of_instances = "6",
    file = ["user-data.service-leader", "user-data.service-manager"]
}
]
